//
//  CommentTableViewCell.swift
//  TableviewHeader
//
//  Created by Elveen on 11/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var commentTextLabel: UILabel!
    
//    var comment: Comment?
//    {
//        didSet
//        {
//            if let comment = comment
//            {
//                userNameLabel.text = comment.user.toString()
//                userNameLabel.textColor = comment.user.toColor()
//                commentTextLabel.text = comment.text
//            }else
//            {
//                userNameLabel.text = nil
//                commentTextLabel.text = nil
//            }
//        }
//    }
}
