//
//  Comment.swift
//  TableviewHeader
//
//  Created by Elveen on 11/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//



//import Foundation
//import UIKit
//
//struct Comment
//{
//    static func allComments() -> [Comment]
//    {
//        let comments =
//            [
//            Comment(user: .emily, text: "Lorem ipsum dolor sit amet, consectetuer adiposcing eli, sed diam nonumy nibh euismod tincidunt ut leoreet dolore magna aliquam erat."),
//            Comment(user: .brendon, text: "Lorem ipsum dolor sit amet, consectetuer adiposcing eli, sed diam nonumy nibh euismod tincidunt ut leoreet dolore magna aliquam erat."),
//            Comment(user: .chalene, text: "Lorem ipsum dolor sit amet, consectetuer adiposcing eli, sed diam nonumy nibh euismod tincidunt ut leoreet dolore magna aliquam erat."),
//            Comment(user: .tony, text: "Lorem ipsum dolor sit amet, consectetuer adiposcing eli, sed diam nonumy nibh euismod tincidunt ut leoreet dolore magna aliquam erat."),
//            Comment(user: .steve, text: "Lorem ipsum dolor sit amet, consectetuer adiposcing eli, sed diam nonumy nibh euismod tincidunt ut leoreet dolore magna aliquam erat."),
//            Comment(user: .mark, text: "Lorem ipsum dolor sit amet, consectetuer adiposcing eli, sed diam nonumy nibh euismod tincidunt ut leoreet dolore magna aliquam erat.")
//            ]
//        return comments
//    }
//
//    enum User
//    {
//        case emily
//        case brendon
//        case chalene
//        case tony
//        case steve
//        case mark
//
//        func toString() -> String
//        {
//            switch self
//            {
//            case .emily:
//                return "emily"
//            case .brendon:
//                return "brendon"
//            case .chalene:
//                return "chalene"
//            case .tony:
//                return "tony"
//            case .steve:
//                return "steve"
//            case .mark:
//                return "mark"
//            default:
//                return "no name"
//            }
//        }
//    }
//}
