//
//  DetailHeaderView.swift
//  TableviewHeader
//
//  Created by Elveen on 11/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class DetailHeaderView: UIView
{
    @IBOutlet weak var imageView: UIImageView!
    
    var image: UIImage?
    {
        didSet
        {
            if let image = image
            {
                imageView.image = image
            }else
            {
                imageView.image = nil
            }
        }
    }
}
