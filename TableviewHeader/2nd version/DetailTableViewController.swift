//
//  DetailTableViewController.swift
//  TableviewHeader
//
//  Created by Elveen on 11/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController
{
    var image: UIImage?
    var category: String?
    //let comments = Comment.allComments()
    
    private let tableHeaderViewHeight : CGFloat = 80.0
    private let tableHEaderViewCutaway: CGFloat = 40.0
    
    var headerView: DetailHeaderView!
    var headerMaskLayer: CAShapeLayer!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationItem.title = category
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
        
        headerView = tableView.tableHeaderView as! DetailHeaderView
        headerView.image = image
        tableView.addSubview(headerView)
        
        tableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -tableHeaderViewHeight + 64)
        
//        headerMaskLayer = CAShapeLayer()
//        headerMaskLayer.fillColor = UIColor.black.cgColor
//        headerView.layer.mask = headerMaskLayer
        
        let effectiveHEight = tableHeaderViewHeight - tableHEaderViewCutaway/2
        tableView.contentInset = UIEdgeInsets(top: effectiveHEight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -effectiveHEight)

        updateHeaderView()
    }
    
    func updateHeaderView()
    {
        let effectiveHeight = tableHeaderViewHeight - tableHEaderViewCutaway/2
        var headerRect = CGRect(x: 0, y: -effectiveHeight, width: tableView.bounds.width, height: tableHeaderViewHeight)

        if tableView.contentOffset.y < -effectiveHeight
        {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y + tableHEaderViewCutaway/2
        }
        headerView.frame = headerRect
    }
}

extension DetailTableViewController
{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5//comments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentTableViewCell
        //cell.comment = comments[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
}

extension DetailTableViewController
{
    override func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        updateHeaderView()
    }
}
