//
//  StretchingHeader.swift
//  TableviewHeader
//
//  Created by Elveen on 11/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import UIKit

class StretchingHeader: UITableViewController
{
    private let headerViewHeight: CGFloat = 180
    private let headerViewCutAway: CGFloat = 30
    
    var headerView: HeaderView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
        
        headerView = tableView.tableHeaderView as? HeaderView
        tableView.tableHeaderView = nil
//        tableView.backgroundColor = UIColor.gray
        tableView.addSubview(headerView)
        
        tableView.contentInset = UIEdgeInsets(top: headerViewHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -headerViewHeight + 64)
        
        let effectiveHeight = headerViewHeight - headerViewCutAway/2
        tableView.contentInset = UIEdgeInsets(top: effectiveHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -effectiveHeight)
        
        updateHeaderView()
    }
    
    func updateHeaderView()
    {
        let effectiveHeight = headerViewHeight - headerViewCutAway/2
        var headerRect = CGRect(x: 0, y: -effectiveHeight, width: tableView.bounds.width, height: headerViewHeight)

        if tableView.contentOffset.y < -effectiveHeight
        {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y + headerViewCutAway/2

        }else
        {
            headerRect.origin.y = tableView.contentOffset.y
        }
        headerView.frame = headerRect
    }
}

extension StretchingHeader
{
    override func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        updateHeaderView()
    }
}

extension StretchingHeader
{
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell
//        return cell
//    }
//
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 70
//    }
}



